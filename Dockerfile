FROM registry.gitlab.com/theoparis/podman-rust:latest as builder

RUN git clone --depth 1 https://github.com/sozu-proxy/sozu /source/
RUN apk add --no-cache libgcc

WORKDIR /source/bin

RUN cargo build --release --features use-openssl
RUN strip /source/target/release/sozu
RUN \
        mkdir -p /tmp/fakeroot/lib  && \
        cp $(ldd /source/target/release/sozu | grep -o '/.\+\.so[^ ]*' | sort | uniq) /tmp/fakeroot/lib && \
        for lib in /tmp/fakeroot/lib/*; do strip --strip-all $lib; done && \
        mkdir /tmp/fakeroot/bin && \
        mkdir -p /tmp/fakeroot/run/sozu

FROM scratch AS release

COPY config/sozu.toml /etc/sozu/config.toml

COPY --from=builder /source/target/release/sozu sozu
COPY --from=builder /tmp/fakeroot/ /

ENV SOZU_CONFIG /etc/sozu/config.toml

VOLUME /etc/sozu

VOLUME /run/sozu

EXPOSE 80
EXPOSE 443

ENTRYPOINT ["/sozu"]

CMD ["start", "-c", "/etc/sozu/config.toml"]

